﻿using UnityEngine;
using System.Collections;

public class MovimentoProjetil : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.GetComponent<Rigidbody>().AddForce(transform.forward * 8000 * Time.deltaTime, ForceMode.Impulse);
    }
	
	// Update is called once per frame
	void Update () {
       
    }
}
